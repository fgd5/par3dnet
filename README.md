# par3dnet

## Prerequisites

- PCL 1.7
- pcl_tools
- Keras 2.1.3
- tensorflow-gpu 1.14.0
- cmake

## Building instruction

1. Clone the repo
2. cd par3dnet/test_tools
3. mkdir build
4. cd build
5. cmake ..
6. make
7. cd ../../
8. python final_predict_par3dnet.py