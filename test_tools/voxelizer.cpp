
#include <pcl/common/common.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/esf.h>
#include <time.h>
#include <glob.h>

using namespace std;
using namespace pcl;
using namespace pcl::io;
using namespace pcl::console;



void preprocessPC(pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud)
{
  // compute the center of the object and translate to the center of the space (object centered in 0,0,0)
  pcl::PointXYZ minPt, maxPt;
  pcl::getMinMax3D (*input_cloud, minPt, maxPt);

  Eigen::Matrix4f transform_1 = Eigen::Matrix4f::Identity();
  transform_1 (0,3) = -(minPt.x + ((maxPt.x - minPt.x)/2));
  transform_1 (1,3) = -(minPt.y + ((maxPt.y - minPt.y)/2));
  transform_1 (2,3) = -(minPt.z + ((maxPt.z - minPt.z)/2));
  pcl::transformPointCloud (*input_cloud, *output_cloud, transform_1);

  // compute the scaling factor and scale to 65x65x65 (max dimension [-32.5,32.5])
  pcl::getMinMax3D (*output_cloud, minPt, maxPt);


  float div = 1.0;
  if(maxPt.x > maxPt.y && maxPt.x > maxPt.z)
  {
    div = maxPt.x;
  } 
  else if (maxPt.y > maxPt.z && maxPt.y > maxPt.z)
  {
    div = maxPt.y;
  } 
  else
  {
    div = maxPt.z;
  }

  for (int i = 0; i < output_cloud->points.size(); i++)
  {
    output_cloud->points[i].x = output_cloud->points[i].x/div*32.5;
    output_cloud->points[i].y = output_cloud->points[i].y/div*32.5;
    output_cloud->points[i].z = output_cloud->points[i].z/div*32.5;
  }

  pcl::getMinMax3D (*output_cloud, minPt, maxPt);
}

template <typename T> T clip(const T& n, const T& lower, const T& upper)
{
  return std::max(lower, std::min(n, upper));
}


int compute_voxel_index ( const float & crOrigin, const float & crPosition, const float & crVoxelSize)
{
  return (int)clip((crPosition - crOrigin)/crVoxelSize, 0.0f, (float)(65-1)); 
}


vector<vector<vector<int>>> createVolumetricRepresentation(pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud, int x, int y, int z)
{
  pcl::PointXYZ minPt, maxPt;
  pcl::getMinMax3D (*input_cloud, minPt, maxPt);

  vector<vector<vector<int>>> rep(65, vector<vector<int>>(65, vector<int>(65, 0)));

  // dunno if this is working, this is copied from other project
  for (auto it = input_cloud->begin(); it != input_cloud->end(); ++it)
  {
    size_t x = compute_voxel_index(minPt.x, (*it).x, 1);
    size_t y = compute_voxel_index(minPt.y, (*it).y, 1);
    size_t z = compute_voxel_index(minPt.z, (*it).z, 1);

    rep[z][y][x] = 1;
  }

  return rep;

}


void
split( vector<string> & theStringVector,  /* Altered/returned value */
       const  string  & theString,
       const  string  & theDelimiter)
{
    size_t  start = 0, end = 0;

    while ( end != string::npos)
    {
        end = theString.find( theDelimiter, start);

        // If at end, use length=maxLength.  Else use length=end-start.
        theStringVector.push_back( theString.substr( start,
                       (end == string::npos) ? string::npos : end - start));

        // If at end, use start=maxSize.  Else use start=end+delimiter.
        start = (   ( end > (string::npos - theDelimiter.size()) )
                  ?  string::npos  :  end + theDelimiter.size());
    }
}



void saveData( vector<vector<vector<int>>> rep, string filename)
{
  // save the data
  ofstream xSave;
  xSave.open (filename); 
  for(int i = 0; i < 65; i++)
  {
    for(int j = 0; j < 65; j++)
    {
      for(int k = 0; k < 65; k++)
      {
        xSave << std::to_string(rep[i][j][k]) << "\n";
      }
    }
  }
  xSave.close();
}


int main (int argc, char **argv)
{

  std::string filenamePcd = argv[1];


  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::io::loadPCDFile<pcl::PointXYZ> (filenamePcd, *cloud);

  pcl::PointCloud<pcl::PointXYZ>::Ptr preprocessed_cloud (new pcl::PointCloud<pcl::PointXYZ>);
  preprocessPC(cloud, preprocessed_cloud);

  vector<vector<vector<int>>> rep = createVolumetricRepresentation(preprocessed_cloud, 65,65,65);
  saveData( rep, "tmp.occ");

  
}
