import numpy as np
np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})
import os
import time
import sys
from keras.optimizers import Adam


# architecture of the network
_PATH_TO_TEST = "."
sys.path.append(_PATH_TO_TEST+"/par3dnet")
import par3dnet


def parseDataXFile(dataXFile):
	datax = []

	f = open(dataXFile, "r")
	for line in f:
		datax.append(float(line.split("\n")[0]))

	datax = np.array(datax).reshape(65,65,65)

	return datax



# set the labels
labels = ['bathtub','bed','chair','desk','dresser','monitor','night','sofa','table','toilet']


# create the network
model = par3dnet.Par3dNet()
opt = Adam(lr=0.0001)
model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
model.load_weights(_PATH_TO_TEST+"/snapshots/fs_new_arch_2818.h5") # '''



datax_file = _PATH_TO_TEST+"/test_data/chair_wheels_front.pcd"


# generate the voxelized representation
cmd = _PATH_TO_TEST+"/test_tools/build/voxelizer "+datax_file
os.system(cmd) 


# load the generated voxelized representation
data = parseDataXFile("tmp.occ")


# feed the network with the voxelized representation of the input point cloud
print  "Predicting...."
tI = time.time()
res = model.predict(x=data.reshape(1,65,65,65,1), verbose=0)
tE = time.time()


print " f(x)=",labels[res[0][0].argmax()],"-", res[0][0]
print " p(x)=", res[1][0]
print " Time Elapsed=", ((tE-tI)*1000),"milliseconds"


# spawn a 3D viewer
cmd = "pcl_viewer -ax 0.2 "+datax_file
os.system(cmd) # '''



