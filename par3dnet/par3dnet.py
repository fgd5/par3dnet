from keras.layers import merge, Input
from keras.layers import Dense, Activation, Flatten
from keras.layers import Conv3D
from keras.layers import Dropout
from keras.models import Model
from keras.preprocessing import image
import keras.backend as K
from keras.utils.layer_utils import convert_all_kernels_in_model
from keras.utils.data_utils import get_file


def build_classoutput(inputs):

     x = Flatten()(inputs)
     x = Dropout(0.2)(x)
     x = Dense(10, activation='softmax', name='category_output', trainable=True)(x)

     return x


def build_poseoutput(inputs):
     x = Flatten()(inputs)
     #x = Dense(1000, activation='relu', name='hidden_pose_output')(inputs)
     x = Dropout(0.2)(x)
     x = Dense(4, activation='tanh', name='pose_output')(x)

     return x


def build_convolutional_first(inputs):
    x = Conv3D(100, (5,5,5), strides=(2, 2, 2), padding='valid', activation='relu', use_bias=True, kernel_initializer='glorot_uniform', trainable=True)(inputs)
    x = Dropout(0.2)(x)
    x = Conv3D(100, (3,3,3), strides=(2, 2, 2), padding='valid', activation='relu', use_bias=True, kernel_initializer='glorot_uniform', trainable=True)(x)

    return x



def build_convolutional_second(inputs):
    x = Conv3D(64, (3,3,3), strides=(2, 2, 2), padding='valid', activation='relu', use_bias=True, kernel_initializer='glorot_uniform', trainable=True)(inputs)
    x = Dropout(0.2)(x)
    x = Conv3D(64, (1,1,1), strides=(1, 1, 1), padding='valid', activation='relu', use_bias=True, kernel_initializer='glorot_uniform', trainable=True)(x)

    return x

def Par3dNet():

    img_input = Input(shape=(65,65,65,1))

    conv_first = build_convolutional_first(img_input)
    conv_second = build_convolutional_second(conv_first)
    class_out = build_classoutput(conv_second)
    pose_out = build_poseoutput(conv_first)

    model = Model(
            inputs=img_input,
            outputs=[class_out, pose_out],
            name="pointnet"
            )

    return model
